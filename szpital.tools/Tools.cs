﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;

namespace szpital.tools
{
    public static class Tools
    {
        /// <summary>
        /// Converst plaintext into MD5
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string GetMD5(string input)
        {
            MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
            md5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(input));
            byte[] result = md5.Hash;
            StringBuilder str = new StringBuilder();
            for (int i = 0; i < result.Length; i++)
            {
                str.Append(result[i].ToString("x2"));
            }
            return str.ToString();
        }

        /// <summary>
        /// Simply validator of PESEL
        /// </summary>
        /// <param name="PESEL"></param>
        /// <returns></returns>
        public static bool CheckPesel(string PESEL)
        {
            if (Int64.TryParse(PESEL, out Int64 result) == false)
                return false;

            if (PESEL.Length != 11)
                return false;

            int[] importances = { 9, 7, 3, 1, 9, 7, 3, 1, 9, 7, 0 };
            int sum = PESEL.Zip(importances, (digit, importance) => (digit - '0') * importance).Sum();

            if (sum % 10 != PESEL[10] - '0')
                return false;

            return true;
        }

        /// <summary>
        /// Converter for date in format YYYY-MM-DD
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        static public DateTime DateFromString(string date)
        {
            string[] temp = date.Split('-');
            int year = int.Parse(temp[0]);
            int month = int.Parse(temp[1]);
            int day = int.Parse(temp[2]);
            return new DateTime(year, month, day);
        }


    }
}
