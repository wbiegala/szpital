﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace szpital.core
{
    public class Role
    {
        private int databaseId;
        private string name;
        private bool isMedicalStaff;

        public string Name { get => name; }
        public bool IsMedicalStaff { get => isMedicalStaff; }
        public int DatabaseId { get => databaseId; }

        public Role(int databaseId, string name, bool isMedicalStaff)
        {
            this.databaseId = databaseId;
            this.name = name;
            this.isMedicalStaff = isMedicalStaff;
        }

    }
}
