﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace szpital.core
{
    public interface IMedicalStaff
    {
        string FirstName { get; }
        string LastName { get; }
        string PESEL { get; }
        Role Role { get; }
        string Spec { get; }
        string Licence { get; }

        List<Duty> GetDutyPlan();
    }
}
