﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Oracle.ManagedDataAccess.Client;

namespace szpital.core
{
    public class Worker
    {
        protected string pesel;
        protected string firstName;
        protected string lastName;
        protected string username;
        protected string password;
        protected Role role;

        public string PESEL { get => pesel; }
        public string FirstName { get => firstName; }
        public string LastName { get => lastName; }
        public string Username { get => username; }
        public string Password { get => password; }
        public Role Role { get => role; }


        public Worker(string pesel, string fName, string lName, string user, string password, Role role)
        {
            this.pesel = pesel;
            this.firstName = fName;
            this.lastName = lName;
            this.username = user;
            this.password = password;
            this.role = role;
        }

        /// <summary>
        /// Save all fields to database with where PESEL = ''
        /// </summary>
        /// <param name="pesel"></param>
        /// <param name="fName"></param>
        /// <param name="lName"></param>
        /// <param name="user"></param>
        /// <param name="password"></param>
        /// <param name="role"></param>
        public void Update(string fName, string lName, string user, string passwordToChange, Role roletoChange)
        {
            int check = 0;
            string SQL = "update WORKER set ";
            if (this.firstName != fName)
            {
                SQL += "FIRST_NAME='" + fName + "', ";
                check++;
            }
            if (this.lastName != lName)
            {
                SQL += "LAST_NAME='" + lName +"', ";
                check++;
            }
            if (this.username != user)
            {
                SQL += "USERNAME='" + user +"', ";
                check++;
            }
            if (this.password != passwordToChange)
            {
                SQL += "PASSWORD='" + passwordToChange + "', ";
                check++;
            }
            if (this.role != roletoChange)
            {
                SQL += "ROLE_ID='" + roletoChange.DatabaseId +"', ";
                check++;
            }

            SQL = SQL.Remove(SQL.Length - 2);
            SQL += " where PESEL='" + this.pesel + "'";

            if (check == 0)
                return;

            this.firstName = fName;
            this.lastName = lName;
            this.username = user;
            this.password = passwordToChange;
            this.role = roletoChange;

            database.OracleDriver connection = new database.OracleDriver();
            using (OracleCommand command = new OracleCommand(SQL, connection.Database))
            {
                command.ExecuteNonQuery();
            }

        }

        public void Insert()
        {
            string SQL = "insert into WORKER (PESEL, FIRST_NAME, LAST_NAME, USERNAME, PASSWORD, ROLE_ID) values (" +
                "'" + this.pesel + "', " +
                "'" + this.firstName + "', " +
                "'" + this.lastName + "', " +
                "'" + this.username + "', " +
                "'" + this.password + "', " +
                "(select ID from ROLE where NAME = '" + this.role.Name + "'))";

            database.OracleDriver connection = new database.OracleDriver();
            using (OracleCommand command = new OracleCommand(SQL, connection.Database))
            {
                command.ExecuteNonQuery();
            }

        }

        public void Delete()
        {
            string SQL = "update WORKER set IS_DELETED='T' where PESEL='" + this.pesel + "'";

            database.OracleDriver connection = new database.OracleDriver();
            using (OracleCommand command = new OracleCommand(SQL, connection.Database))
            {
                command.ExecuteNonQuery();
            }
        }


    }
}
