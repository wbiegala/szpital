﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Oracle.ManagedDataAccess.Client;

namespace szpital.core
{
    public class Doctor : Worker, IMedicalStaff
    {
        private string licence;
        private string spec;

        public string Licence { get => licence; }
        public string Spec { get => spec; }

        public Doctor (string pesel, string fName, string lName, string user, string password, Role role, string licence, string spec) 
            : base(pesel, fName, lName, user, password, role)
        {
            this.licence = licence;
            this.spec = spec;
        }

        public List<Duty> GetDutyPlan()
        {
            List<Duty> result = new List<Duty>();
            string SQL = "select TO_CHAR(\"DATE\", 'YYYY-mm-DD') as \"DATA\" from DUTY where IS_ACTIVE='T' and WORKER_PESEL='" + this.pesel + "'";

            database.OracleDriver connection = new database.OracleDriver();
            using (OracleCommand command = new OracleCommand(SQL, connection.Database))
            {
                OracleDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    result.Add(new Duty(tools.Tools.DateFromString(reader["DATA"].ToString()), this));
                }
            }
            return result;
        }
    }
}
