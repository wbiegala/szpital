﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Oracle.ManagedDataAccess.Client;

namespace szpital.core
{
    public class Nurse : Worker, IMedicalStaff
    {
        public Nurse(string pesel, string fName, string lName, string user, string password, Role role) 
            : base(pesel, fName, lName, user, password, role)
        {
            
        }

        public string Spec { get => ""; }

        public string Licence { get => ""; }

        public List<Duty> GetDutyPlan()
        {
            List<Duty> result = new List<Duty>();
            string SQL = "select TO_CHAR(\"DATE\", 'YYYY-mm-DD') as \"DATA\" from DUTY where IS_ACTIVE='T' and WORKER_PESEL='" + this.pesel + "'";

            database.OracleDriver connection = new database.OracleDriver();
            using (OracleCommand command = new OracleCommand(SQL, connection.Database))
            {
                OracleDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    result.Add(new Duty(tools.Tools.DateFromString(reader["DATA"].ToString()), this));
                }
            }
            return result;
        }
    }
}
