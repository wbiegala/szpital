﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Oracle.ManagedDataAccess.Client;

namespace szpital.core
{
    public class Duty
    {
        private DateTime day;
        private IMedicalStaff worker;

        public DateTime Day { get => day; }
        public IMedicalStaff Worker { get => worker; }

        public Duty(DateTime day, IMedicalStaff worker)
        {
            this.day = day;
            this.worker = worker;
        }

        public void Insert()
        {
            string SQL = "insert into DUTY (\"DATE\", WORKER_PESEL) values (TO_DATE('" + 
                this.day.ToString("yyyy/MM/dd") + "','yyyy/mm/dd'), '" + 
                this.worker.PESEL + "')";

            database.OracleDriver connection = new database.OracleDriver();
            using (OracleCommand command = new OracleCommand(SQL, connection.Database))
            {
                command.ExecuteNonQuery();
            }
        }

        public void Delete()
        {
            string SQL = "update duty set IS_ACTIVE = 'N' " +
                "where " +
                "\"DATE\"=TO_DATE('" + this.day.ToString("yyyy/MM/dd") + "','yyyy/mm/dd') AND " +
                "WORKER_PESEL='" + this.worker.PESEL + "'";

            database.OracleDriver connection = new database.OracleDriver();
            using (OracleCommand command = new OracleCommand(SQL, connection.Database))
            {
                command.ExecuteNonQuery();
            }
        }



    }
}
