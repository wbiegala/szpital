﻿namespace szpital
{
    partial class WorkerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.primaryDataGroupBox = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.roleComboBox = new System.Windows.Forms.ComboBox();
            this.systemDataGroupBox = new System.Windows.Forms.GroupBox();
            this.confirmPasswordLabel = new System.Windows.Forms.Label();
            this.newPasswordLabel = new System.Windows.Forms.Label();
            this.confirmPasswordTextBox = new System.Windows.Forms.TextBox();
            this.newPasswordTextBox = new System.Windows.Forms.TextBox();
            this.changePasswordButton = new System.Windows.Forms.Button();
            this.usernameLabel = new System.Windows.Forms.Label();
            this.usernameTextBox = new System.Windows.Forms.TextBox();
            this.lastNameLabel = new System.Windows.Forms.Label();
            this.firstNameLabel = new System.Windows.Forms.Label();
            this.lastNameTextBox = new System.Windows.Forms.TextBox();
            this.firstNameTextBox = new System.Windows.Forms.TextBox();
            this.peselLabel = new System.Windows.Forms.Label();
            this.peselTextBox = new System.Windows.Forms.TextBox();
            this.extendedDataGroupBox = new System.Windows.Forms.GroupBox();
            this.docSpecComboBox = new System.Windows.Forms.Label();
            this.licenceLabel = new System.Windows.Forms.Label();
            this.specComboBox = new System.Windows.Forms.ComboBox();
            this.licenceTextBox = new System.Windows.Forms.TextBox();
            this.SaveButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.primaryDataGroupBox.SuspendLayout();
            this.systemDataGroupBox.SuspendLayout();
            this.extendedDataGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // primaryDataGroupBox
            // 
            this.primaryDataGroupBox.Controls.Add(this.label1);
            this.primaryDataGroupBox.Controls.Add(this.roleComboBox);
            this.primaryDataGroupBox.Controls.Add(this.systemDataGroupBox);
            this.primaryDataGroupBox.Controls.Add(this.lastNameLabel);
            this.primaryDataGroupBox.Controls.Add(this.firstNameLabel);
            this.primaryDataGroupBox.Controls.Add(this.lastNameTextBox);
            this.primaryDataGroupBox.Controls.Add(this.firstNameTextBox);
            this.primaryDataGroupBox.Controls.Add(this.peselLabel);
            this.primaryDataGroupBox.Controls.Add(this.peselTextBox);
            this.primaryDataGroupBox.Location = new System.Drawing.Point(12, 12);
            this.primaryDataGroupBox.Name = "primaryDataGroupBox";
            this.primaryDataGroupBox.Size = new System.Drawing.Size(548, 166);
            this.primaryDataGroupBox.TabIndex = 0;
            this.primaryDataGroupBox.TabStop = false;
            this.primaryDataGroupBox.Text = "Dane podstawowe";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 122);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Stanowisko";
            // 
            // roleComboBox
            // 
            this.roleComboBox.FormattingEnabled = true;
            this.roleComboBox.Location = new System.Drawing.Point(6, 138);
            this.roleComboBox.Name = "roleComboBox";
            this.roleComboBox.Size = new System.Drawing.Size(284, 21);
            this.roleComboBox.TabIndex = 7;
            this.roleComboBox.SelectedIndexChanged += new System.EventHandler(this.roleComboBox_SelectedIndexChanged);
            // 
            // systemDataGroupBox
            // 
            this.systemDataGroupBox.Controls.Add(this.confirmPasswordLabel);
            this.systemDataGroupBox.Controls.Add(this.newPasswordLabel);
            this.systemDataGroupBox.Controls.Add(this.confirmPasswordTextBox);
            this.systemDataGroupBox.Controls.Add(this.newPasswordTextBox);
            this.systemDataGroupBox.Controls.Add(this.changePasswordButton);
            this.systemDataGroupBox.Controls.Add(this.usernameLabel);
            this.systemDataGroupBox.Controls.Add(this.usernameTextBox);
            this.systemDataGroupBox.Location = new System.Drawing.Point(296, 19);
            this.systemDataGroupBox.Name = "systemDataGroupBox";
            this.systemDataGroupBox.Size = new System.Drawing.Size(246, 140);
            this.systemDataGroupBox.TabIndex = 6;
            this.systemDataGroupBox.TabStop = false;
            this.systemDataGroupBox.Text = "Dane systemowe";
            // 
            // confirmPasswordLabel
            // 
            this.confirmPasswordLabel.AutoSize = true;
            this.confirmPasswordLabel.Location = new System.Drawing.Point(125, 93);
            this.confirmPasswordLabel.Name = "confirmPasswordLabel";
            this.confirmPasswordLabel.Size = new System.Drawing.Size(73, 13);
            this.confirmPasswordLabel.TabIndex = 6;
            this.confirmPasswordLabel.Text = "Potwierdzenie";
            this.confirmPasswordLabel.Visible = false;
            // 
            // newPasswordLabel
            // 
            this.newPasswordLabel.AutoSize = true;
            this.newPasswordLabel.Location = new System.Drawing.Point(3, 93);
            this.newPasswordLabel.Name = "newPasswordLabel";
            this.newPasswordLabel.Size = new System.Drawing.Size(65, 13);
            this.newPasswordLabel.TabIndex = 5;
            this.newPasswordLabel.Text = "Nowe hasło";
            this.newPasswordLabel.Visible = false;
            // 
            // confirmPasswordTextBox
            // 
            this.confirmPasswordTextBox.Location = new System.Drawing.Point(128, 109);
            this.confirmPasswordTextBox.MaxLength = 40;
            this.confirmPasswordTextBox.Name = "confirmPasswordTextBox";
            this.confirmPasswordTextBox.Size = new System.Drawing.Size(112, 20);
            this.confirmPasswordTextBox.TabIndex = 4;
            this.confirmPasswordTextBox.UseSystemPasswordChar = true;
            this.confirmPasswordTextBox.Visible = false;
            // 
            // newPasswordTextBox
            // 
            this.newPasswordTextBox.Location = new System.Drawing.Point(6, 109);
            this.newPasswordTextBox.MaxLength = 40;
            this.newPasswordTextBox.Name = "newPasswordTextBox";
            this.newPasswordTextBox.Size = new System.Drawing.Size(116, 20);
            this.newPasswordTextBox.TabIndex = 3;
            this.newPasswordTextBox.UseSystemPasswordChar = true;
            this.newPasswordTextBox.Visible = false;
            // 
            // changePasswordButton
            // 
            this.changePasswordButton.Location = new System.Drawing.Point(85, 67);
            this.changePasswordButton.Name = "changePasswordButton";
            this.changePasswordButton.Size = new System.Drawing.Size(75, 23);
            this.changePasswordButton.TabIndex = 2;
            this.changePasswordButton.Text = "Zmień hasło";
            this.changePasswordButton.UseVisualStyleBackColor = true;
            this.changePasswordButton.Click += new System.EventHandler(this.changePasswordButton_Click);
            // 
            // usernameLabel
            // 
            this.usernameLabel.AutoSize = true;
            this.usernameLabel.Location = new System.Drawing.Point(3, 25);
            this.usernameLabel.Name = "usernameLabel";
            this.usernameLabel.Size = new System.Drawing.Size(102, 13);
            this.usernameLabel.TabIndex = 1;
            this.usernameLabel.Text = "Nazwa użytkownika";
            // 
            // usernameTextBox
            // 
            this.usernameTextBox.Location = new System.Drawing.Point(6, 41);
            this.usernameTextBox.MaxLength = 40;
            this.usernameTextBox.Name = "usernameTextBox";
            this.usernameTextBox.Size = new System.Drawing.Size(234, 20);
            this.usernameTextBox.TabIndex = 0;
            // 
            // lastNameLabel
            // 
            this.lastNameLabel.AutoSize = true;
            this.lastNameLabel.Location = new System.Drawing.Point(152, 73);
            this.lastNameLabel.Name = "lastNameLabel";
            this.lastNameLabel.Size = new System.Drawing.Size(53, 13);
            this.lastNameLabel.TabIndex = 5;
            this.lastNameLabel.Text = "Nazwisko";
            // 
            // firstNameLabel
            // 
            this.firstNameLabel.AutoSize = true;
            this.firstNameLabel.Location = new System.Drawing.Point(3, 73);
            this.firstNameLabel.Name = "firstNameLabel";
            this.firstNameLabel.Size = new System.Drawing.Size(26, 13);
            this.firstNameLabel.TabIndex = 4;
            this.firstNameLabel.Text = "Imię";
            // 
            // lastNameTextBox
            // 
            this.lastNameTextBox.Location = new System.Drawing.Point(155, 89);
            this.lastNameTextBox.MaxLength = 40;
            this.lastNameTextBox.Name = "lastNameTextBox";
            this.lastNameTextBox.Size = new System.Drawing.Size(135, 20);
            this.lastNameTextBox.TabIndex = 3;
            // 
            // firstNameTextBox
            // 
            this.firstNameTextBox.Location = new System.Drawing.Point(6, 89);
            this.firstNameTextBox.MaxLength = 40;
            this.firstNameTextBox.Name = "firstNameTextBox";
            this.firstNameTextBox.Size = new System.Drawing.Size(131, 20);
            this.firstNameTextBox.TabIndex = 2;
            // 
            // peselLabel
            // 
            this.peselLabel.AutoSize = true;
            this.peselLabel.Location = new System.Drawing.Point(3, 27);
            this.peselLabel.Name = "peselLabel";
            this.peselLabel.Size = new System.Drawing.Size(41, 13);
            this.peselLabel.TabIndex = 1;
            this.peselLabel.Text = "PESEL";
            // 
            // peselTextBox
            // 
            this.peselTextBox.Location = new System.Drawing.Point(6, 43);
            this.peselTextBox.MaxLength = 11;
            this.peselTextBox.Name = "peselTextBox";
            this.peselTextBox.ReadOnly = true;
            this.peselTextBox.Size = new System.Drawing.Size(131, 20);
            this.peselTextBox.TabIndex = 0;
            // 
            // extendedDataGroupBox
            // 
            this.extendedDataGroupBox.Controls.Add(this.docSpecComboBox);
            this.extendedDataGroupBox.Controls.Add(this.licenceLabel);
            this.extendedDataGroupBox.Controls.Add(this.specComboBox);
            this.extendedDataGroupBox.Controls.Add(this.licenceTextBox);
            this.extendedDataGroupBox.Location = new System.Drawing.Point(12, 184);
            this.extendedDataGroupBox.Name = "extendedDataGroupBox";
            this.extendedDataGroupBox.Size = new System.Drawing.Size(548, 69);
            this.extendedDataGroupBox.TabIndex = 1;
            this.extendedDataGroupBox.TabStop = false;
            this.extendedDataGroupBox.Text = "Dane rozszerzone";
            this.extendedDataGroupBox.Visible = false;
            // 
            // docSpecComboBox
            // 
            this.docSpecComboBox.AutoSize = true;
            this.docSpecComboBox.Location = new System.Drawing.Point(268, 22);
            this.docSpecComboBox.Name = "docSpecComboBox";
            this.docSpecComboBox.Size = new System.Drawing.Size(69, 13);
            this.docSpecComboBox.TabIndex = 3;
            this.docSpecComboBox.Text = "Specjalizacja";
            // 
            // licenceLabel
            // 
            this.licenceLabel.AutoSize = true;
            this.licenceLabel.Location = new System.Drawing.Point(6, 22);
            this.licenceLabel.Name = "licenceLabel";
            this.licenceLabel.Size = new System.Drawing.Size(66, 13);
            this.licenceLabel.TabIndex = 2;
            this.licenceLabel.Text = "Numer PWZ";
            // 
            // specComboBox
            // 
            this.specComboBox.FormattingEnabled = true;
            this.specComboBox.Location = new System.Drawing.Point(271, 37);
            this.specComboBox.Name = "specComboBox";
            this.specComboBox.Size = new System.Drawing.Size(271, 21);
            this.specComboBox.TabIndex = 1;
            // 
            // licenceTextBox
            // 
            this.licenceTextBox.Location = new System.Drawing.Point(6, 38);
            this.licenceTextBox.Name = "licenceTextBox";
            this.licenceTextBox.Size = new System.Drawing.Size(235, 20);
            this.licenceTextBox.TabIndex = 0;
            // 
            // SaveButton
            // 
            this.SaveButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SaveButton.Location = new System.Drawing.Point(471, 259);
            this.SaveButton.Name = "SaveButton";
            this.SaveButton.Size = new System.Drawing.Size(89, 31);
            this.SaveButton.TabIndex = 2;
            this.SaveButton.Text = "Zapisz";
            this.SaveButton.UseVisualStyleBackColor = true;
            this.SaveButton.Click += new System.EventHandler(this.SaveButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.Location = new System.Drawing.Point(376, 259);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(89, 31);
            this.cancelButton.TabIndex = 3;
            this.cancelButton.Text = "Anuluj";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // WorkerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(572, 298);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.SaveButton);
            this.Controls.Add(this.extendedDataGroupBox);
            this.Controls.Add(this.primaryDataGroupBox);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(588, 337);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(588, 337);
            this.Name = "WorkerForm";
            this.ShowIcon = false;
            this.Text = "Pracownik: ";
            this.primaryDataGroupBox.ResumeLayout(false);
            this.primaryDataGroupBox.PerformLayout();
            this.systemDataGroupBox.ResumeLayout(false);
            this.systemDataGroupBox.PerformLayout();
            this.extendedDataGroupBox.ResumeLayout(false);
            this.extendedDataGroupBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox primaryDataGroupBox;
        private System.Windows.Forms.GroupBox extendedDataGroupBox;
        private System.Windows.Forms.Label peselLabel;
        private System.Windows.Forms.TextBox peselTextBox;
        private System.Windows.Forms.TextBox lastNameTextBox;
        private System.Windows.Forms.TextBox firstNameTextBox;
        private System.Windows.Forms.Label lastNameLabel;
        private System.Windows.Forms.Label firstNameLabel;
        private System.Windows.Forms.GroupBox systemDataGroupBox;
        private System.Windows.Forms.Label usernameLabel;
        private System.Windows.Forms.TextBox usernameTextBox;
        private System.Windows.Forms.Button changePasswordButton;
        private System.Windows.Forms.TextBox confirmPasswordTextBox;
        private System.Windows.Forms.TextBox newPasswordTextBox;
        private System.Windows.Forms.Label confirmPasswordLabel;
        private System.Windows.Forms.Label newPasswordLabel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox roleComboBox;
        private System.Windows.Forms.TextBox licenceTextBox;
        private System.Windows.Forms.Label docSpecComboBox;
        private System.Windows.Forms.Label licenceLabel;
        private System.Windows.Forms.ComboBox specComboBox;
        private System.Windows.Forms.Button SaveButton;
        private System.Windows.Forms.Button cancelButton;
    }
}