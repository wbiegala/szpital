﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Oracle.ManagedDataAccess.Client;

namespace szpital
{
    public partial class WorkerBrowser : Form
    {
        private bool isAdminMode;
        private List<core.IMedicalStaff> medicalStaff;
        private List<core.Worker> workers;

        public WorkerBrowser(bool mode)
        {
            InitializeComponent();
            this.isAdminMode = mode;
            this.newButton.Visible = isAdminMode;
            this.deleteButton.Visible = isAdminMode;
            this.showButton.Visible = isAdminMode;

            if (isAdminMode == false)
            {
                medicalStaff = new List<core.IMedicalStaff>(getAllMedicalWorkers());
                refreshMedicalList(medicalStaff);
            }
            else
            {
                workers = new List<core.Worker>(getAllWorkers());
                refreshWorkerList(workers);
            }
        }

        //private List<core.Worker>



        private List<core.Worker> getAllWorkers()
        {
            List<core.Worker> temp = new List<core.Worker>();
            try
            {
                string SQL = @"select PESEL, FIRST_NAME, LAST_NAME, PASSWORD, USERNAME, r.NAME as ROLE
                            from WORKER w
                            inner join ROLE r on w.ROLE_ID = r.ID
                            where w.IS_DELETED = 'N'
                            order by LAST_NAME";

                using (OracleCommand command = new OracleCommand(SQL, Program.Connection.Database))
                {
                    OracleDataReader reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        temp.Add(new core.Worker(
                            reader["PESEL"].ToString(),
                            reader["FIRST_NAME"].ToString(),
                            reader["LAST_NAME"].ToString(),
                            reader["USERNAME"].ToString(),
                            reader["PASSWORD"].ToString(),
                            Program.Roles.Find(x => x.Name == reader["ROLE"].ToString())));
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return temp;
        }

        private void refreshWorkerList(List<core.Worker> toDisplay)
        {
            workerListView.Items.Clear();
            workerListView.Columns.Clear();
            workerListView.View = View.Details;
            workerListView.GridLines = true;

            workerListView.Columns.Add("PESEL", 100);
            workerListView.Columns.Add("Imię", 150);
            workerListView.Columns.Add("Nazwisko", 150);
            workerListView.Columns.Add("Stanowisko", 120);
            workerListView.Columns.Add("Nazwa użytkownika", 150);

            foreach (var worker in toDisplay)
            {
                ListViewItem item = new ListViewItem(new[] { worker.PESEL, worker.FirstName, worker.LastName, worker.Role.Name, worker.Username });
                workerListView.Items.Add(item);
            }
        }











        // Medical
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////


        /// <summary>
        /// Gets data about medical workers from database and load data to 'medicalStaff'
        /// </summary>
        /// <returns></returns>
        private List<core.IMedicalStaff> getAllMedicalWorkers()
        {
            List<core.IMedicalStaff> temp = new List<core.IMedicalStaff>();
            try
            {
                string SQL = @"select w.PESEL, w.FIRST_NAME, w.LAST_NAME, r.NAME as ROLE
                            from WORKER w
                            inner join ROLE r on w.role_id = r.id
                            where r.is_medical_staff = 'T'
                            and w.IS_DELETED = 'N'";

                using (OracleCommand mainCommand = new OracleCommand(SQL, Program.Connection.Database))
                {
                    OracleDataReader mainReader = mainCommand.ExecuteReader();
                    while (mainReader.Read())
                    {
                        if (mainReader["ROLE"].ToString() == "Pielęgniarka")
                        {
                            temp.Add(new core.Nurse(
                                mainReader["PESEL"].ToString(),
                                mainReader["FIRST_NAME"].ToString(),
                                mainReader["LAST_NAME"].ToString(),
                                "", "",
                                Program.Roles.Find(x => x.Name == mainReader["ROLE"].ToString())));
                        }
                        else if (mainReader["ROLE"].ToString() == "Lekarz")
                        {
                            string pesel = mainReader["PESEL"].ToString();

                            string SQL2 = @"select d.LICENCE, ds.NAME as SPEC
                                        from DOCTOR_DATA d
                                        inner join DOC_SPEC ds on d.DOC_SPEC_ID = ds.ID
                                        where d.WORKER_PESEL='" + pesel + "'";

                            string licence = "", spec = "";

                            using (OracleCommand addCommand = new OracleCommand(SQL2, Program.Connection.Database))
                            {
                                OracleDataReader addReader = addCommand.ExecuteReader();
                                addReader.Read();
                                licence = addReader["LICENCE"].ToString();
                                spec = addReader["SPEC"].ToString();
                            }

                            temp.Add(new core.Doctor(
                                pesel,
                                mainReader["FIRST_NAME"].ToString(),
                                mainReader["LAST_NAME"].ToString(),
                                "", "",
                                Program.Roles.Find(x => x.Name == mainReader["ROLE"].ToString()),
                                licence,
                                spec));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return temp;
        }

        /// <summary>
        /// Refresh 'workerListView' for madical users logged in.
        /// </summary>
        /// <param name="toDisplay"></param>
        private void refreshMedicalList(List<core.IMedicalStaff> toDisplay)
        {
            workerListView.Items.Clear();
            workerListView.Columns.Clear();
            workerListView.View = View.Details;
            workerListView.GridLines = true;

            workerListView.Columns.Add("PESEL", 100);
            workerListView.Columns.Add("Imię", 150);
            workerListView.Columns.Add("Nazwisko", 150);
            workerListView.Columns.Add("Stanowisko", 120);
            workerListView.Columns.Add("Numer PWZ", 100);
            workerListView.Columns.Add("Specjalizacja", 100);

            foreach (var worker in toDisplay)
            {
                ListViewItem item = new ListViewItem(new[] { worker.PESEL, worker.FirstName, worker.LastName, worker.Role.Name, worker.Licence, worker.Spec });
                workerListView.Items.Add(item);
            }
        }




        private void searchButton_Click(object sender, EventArgs e)
        {
            if (searchTextBox.Text.Length > 2)
            {
                if (isAdminMode == true)
                {
                    List<core.Worker> temp = workers.FindAll(x => (x.PESEL == searchTextBox.Text) || (x.LastName == searchTextBox.Text) || (x.Username == searchTextBox.Text));
                    refreshWorkerList(temp);
                }
                else
                {
                    List<core.IMedicalStaff> temp = medicalStaff.FindAll(x => (x.PESEL == searchTextBox.Text) || (x.LastName == searchTextBox.Text) || (x.Spec == searchTextBox.Text));
                    refreshMedicalList(temp);
                }
            }
        }

        private void allButton_Click(object sender, EventArgs e)
        {
            searchTextBox.Clear();

            if (isAdminMode == true)
            {
                refreshWorkerList(workers);
            }
            else
            {
                refreshMedicalList(medicalStaff);
            }
        }


        /// <summary>
        /// Adding new worker - show window
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void newButton_Click(object sender, EventArgs e)
        {
            if (isAdminMode == true)
            {
                WorkerForm worker = new WorkerForm();

                if (this.WindowState == FormWindowState.Maximized)
                {
                    this.WindowState = FormWindowState.Normal;
                    this.Dock = DockStyle.Fill;
                }
                this.Enabled = false;

                worker.MdiParent = this.MdiParent;
                worker.TopMost = true;
                worker.UserDataChanged += OnUserAdded;
                worker.FormClosed += ChildFormClosed;
                worker.Show();
            }
        }

        public void OnUserDataChanged(object o, core.Worker user)
        {
            this.Enabled = true;
            refreshWorkerList(this.workers);
        }

        public void OnUserAdded(object o, core.Worker user)
        {
            this.Enabled = true;
            this.workers.Add(user);
            refreshWorkerList(this.workers);
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            try
            {
                core.Worker selected = workers.Find(x => x.PESEL == workerListView.SelectedItems[0].Text);

                string warningContent = "Czy napewno chcesz usunąć wybranego użytkownika?";

                if (selected.Role.IsMedicalStaff == true)
                {
                    warningContent += "\nJeśli użytkownik posiada ustawione dyżury to zostaną one usunięte.";
                }

                DialogResult warning = MessageBox.Show(warningContent, "Ostrzeżenie", MessageBoxButtons.YesNo, MessageBoxIcon.Asterisk);

                if (warning == DialogResult.Yes)
                {
                    selected.Delete();
                    workers.Remove(selected);
                    refreshWorkerList(workers);
                }
            }
            catch (ArgumentOutOfRangeException ex)
            {
                MessageBox.Show("Aby wykonać tę opcję musisz zaznaczyć pracownika...", "Wystąpił błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void ChildFormClosed(object o, EventArgs e)
        {
            this.Enabled = true;
        }

        private void showButton_Click(object sender, EventArgs e)
        {
            try
            {
                core.Worker selected = workers.Find(x => x.PESEL == workerListView.SelectedItems[0].Text);

                if (isAdminMode == true)
                {
                    WorkerForm worker = new WorkerForm(selected);

                    if (this.WindowState == FormWindowState.Maximized)
                    {
                        this.WindowState = FormWindowState.Normal;
                        this.Dock = DockStyle.Fill;
                    }
                    this.Enabled = false;

                    worker.MdiParent = this.MdiParent;
                    worker.TopMost = true;
                    worker.UserDataChanged += OnUserDataChanged;
                    worker.FormClosed += ChildFormClosed;
                    worker.Show();


                }
            }
            catch (ArgumentOutOfRangeException ex)
            {
                MessageBox.Show("Aby wykonać tę opcję musisz zaznaczyć pracownika...", "Wystąpił błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}

