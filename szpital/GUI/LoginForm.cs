﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Oracle.ManagedDataAccess.Client;

namespace szpital
{
    public partial class LoginForm : Form
    {
        // event informs MainWindow when user was logged
        public delegate void UserLoggedEventHandler(object o, core.Worker user);
        public event UserLoggedEventHandler UserLogged;


        public LoginForm()
        {
            InitializeComponent();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void loginButton_Click(object sender, EventArgs e)
        {
            Int32 exist;
            try
            {
                string sql_CheckIfExists = "select count(*) as count from WORKER where USERNAME='" + usernameTextBox.Text + "' and PASSWORD='" + tools.Tools.GetMD5(passwordTextBox.Text) + "' and IS_DELETED='N'";
                using (OracleCommand command = new OracleCommand(sql_CheckIfExists, Program.Connection.Database))
                {
                    OracleDataReader reader = command.ExecuteReader();
                    reader.Read();
                    exist = Int32.Parse(reader["count"].ToString());
                }

                if (exist == 1)
                {
                    string sql_BasicData = "select * from WORKER where USERNAME='" + usernameTextBox.Text + "' and PASSWORD='" + tools.Tools.GetMD5(passwordTextBox.Text) + "' and IS_DELETED='N'";

                    using (OracleCommand command = new OracleCommand(sql_BasicData, Program.Connection.Database))
                    {
                        OracleDataReader reader = command.ExecuteReader();
                        reader.Read();
                        Int32 roleId = Int32.Parse(reader["ROLE_ID"].ToString());
                        core.Worker toLog = new core.Worker(
                            reader["PESEL"].ToString(),
                            reader["FIRST_NAME"].ToString(),
                            reader["LAST_NAME"].ToString(),
                            reader["USERNAME"].ToString(),
                            reader["PASSWORD"].ToString(),
                            Program.Roles.Find(x => x.DatabaseId == roleId));
                        OnUserLogged(toLog);
                        this.Close();
                    }

                }
                else if (exist == 0)
                {
                    usernameTextBox.Clear();
                    passwordTextBox.Clear();
                    MessageBox.Show("Błędna nazwa użytkownika lub błędne hasło...", "Błąd logowania", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    usernameTextBox.Clear();
                    passwordTextBox.Clear();
                    MessageBox.Show("Brak spójności bazy danych - zgłoś administratorowi systemu", "Błąd krytyczny!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        /// <summary>
        /// Publisher for event UserLogged. 
        /// </summary>
        /// <param name="user"></param>
        protected virtual void OnUserLogged(core.Worker user)
        {
            UserLogged(this, user);
        }
    }
}
