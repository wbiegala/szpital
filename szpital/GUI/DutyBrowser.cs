﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Oracle.ManagedDataAccess.Client;

namespace szpital
{
    public partial class DutyBrowser : Form
    {
        private List<core.Duty> dutyList;

        public DutyBrowser(bool isAdminMode)
        {
            InitializeComponent();
            if (isAdminMode == false)
            {
                addButton.Visible = false;
                deleteButton.Visible = false;
            }
            this.dutyList = getAllDuties();
            refreshDutyList(this.dutyList);
            setNullFilter();
        }


        private List<core.IMedicalStaff> getAllMedicalWorkers()
        {
            List<core.IMedicalStaff> temp = new List<core.IMedicalStaff>();

            try
            {

                string SQL = @"select w.PESEL, w.FIRST_NAME, w.LAST_NAME, r.NAME as ROLE
                            from WORKER w
                            inner join ROLE r on w.role_id = r.id
                            where r.is_medical_staff = 'T'
                            and w.IS_DELETED = 'N'";

                using (OracleCommand mainCommand = new OracleCommand(SQL, Program.Connection.Database))
                {
                    OracleDataReader mainReader = mainCommand.ExecuteReader();
                    while (mainReader.Read())
                    {
                        if (mainReader["ROLE"].ToString() == "Pielęgniarka")
                        {
                            temp.Add(new core.Nurse(
                                mainReader["PESEL"].ToString(),
                                mainReader["FIRST_NAME"].ToString(),
                                mainReader["LAST_NAME"].ToString(),
                                "", "",
                                Program.Roles.Find(x => x.Name == mainReader["ROLE"].ToString())));
                        }
                        else if (mainReader["ROLE"].ToString() == "Lekarz")
                        {
                            string pesel = mainReader["PESEL"].ToString();

                            string SQL2 = @"select d.LICENCE, ds.NAME as SPEC
                                        from DOCTOR_DATA d
                                        inner join DOC_SPEC ds on d.DOC_SPEC_ID = ds.ID
                                        where d.WORKER_PESEL='" + pesel + "'";

                            string licence = "", spec = "";

                            using (OracleCommand addCommand = new OracleCommand(SQL2, Program.Connection.Database))
                            {
                                OracleDataReader addReader = addCommand.ExecuteReader();
                                addReader.Read();
                                licence = addReader["LICENCE"].ToString();
                                spec = addReader["SPEC"].ToString();
                            }

                            temp.Add(new core.Doctor(
                                pesel,
                                mainReader["FIRST_NAME"].ToString(),
                                mainReader["LAST_NAME"].ToString(),
                                "", "",
                                Program.Roles.Find(x => x.Name == mainReader["ROLE"].ToString()),
                                licence,
                                spec));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return temp;
        }




        private List<core.Duty> getAllDuties()
        {
            List<core.IMedicalStaff> medicals = getAllMedicalWorkers();
            List<core.Duty> result = new List<core.Duty>();

            try
            {
                

                foreach (var worker in medicals)
                {
                    foreach (var duty in worker.GetDutyPlan())
                    {
                        result.Add(duty);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return new List<core.Duty>(result.OrderBy(x => x.Day).ToList());
        }

        private void refreshDutyList(List<core.Duty> toDisplay)
        {
            dutyListView.Columns.Clear();
            dutyListView.Items.Clear();
            dutyListView.View = View.Details;
            dutyListView.GridLines = true;

            dutyListView.Columns.Add("Data", 100);
            dutyListView.Columns.Add("PESEL", 100);
            dutyListView.Columns.Add("Imię", 150);
            dutyListView.Columns.Add("Nazwisko", 150);
            dutyListView.Columns.Add("Stanowisko", 120);
            dutyListView.Columns.Add("Numer PWZ", 100);
            dutyListView.Columns.Add("Specjalizacja", 100);

            foreach (var duty in toDisplay)
            {
                ListViewItem item = new ListViewItem(new[] {
                    duty.Day.ToString("yyyy-MM-dd"),
                    duty.Worker.PESEL,
                    duty.Worker.FirstName,
                    duty.Worker.LastName,
                    duty.Worker.Role.Name,
                    duty.Worker.Licence,
                    duty.Worker.Spec});

                dutyListView.Items.Add(item);
            }

        }

        private void addButton_Click(object sender, EventArgs e)
        {
            this.Enabled = false;
            if (this.WindowState == FormWindowState.Maximized)
            {
                this.WindowState = FormWindowState.Normal;
                this.Dock = DockStyle.Fill;
            }
            DutyAdder dutyAdder = new DutyAdder(this.dutyList.FindAll(x => x.Day >= DateTime.Today), getAllMedicalWorkers());
            dutyAdder.FormClosed += OnChildFormClosed;
            dutyAdder.DutyAdded += OnDutyAdded;
            dutyAdder.Show();
        }


        public void OnDutyAdded(object o, core.Duty duty)
        {
            dutyList.Add(duty);
            setNullFilter();
            refreshDutyList(dutyList);
        }

        public void OnChildFormClosed(object o, EventArgs e)
        {
            this.Enabled = true;
        }

        private void setNullFilter()
        {
            medicalComboBox.Items.Clear();
            foreach (var worker in getAllMedicalWorkers())
            {
                medicalComboBox.Items.Add(worker.PESEL + " - " + worker.FirstName + " - " + worker.LastName + " - " + worker.Role.Name.ToString() + " - " + worker.Spec);
            }
            medicalComboBox.SelectedItem = medicalComboBox.Items[0];
            refreshDutyList(dutyList);
            workerCheckBox.Checked = false;
            fromDatePicker.Value = DateTime.Today;
            toDatePicker.Value = fromDatePicker.Value.AddDays(1);
            filterGroupBox.Text = "Filtr [nieaktywny]";
        }

        private void clearButton_Click(object sender, EventArgs e)
        {
            setNullFilter();
        }

        private void searchButton_Click(object sender, EventArgs e)
        {
            if (fromDatePicker.Value > toDatePicker.Value)
            {
                MessageBox.Show("Data OD jest większa niż data DO, filtr nie ma sensu", "Błąd!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                List<core.Duty> toDisplay = new List<core.Duty>(dutyList);

                if (workerCheckBox.Checked == true)
                {
                    string[] pesel = medicalComboBox.SelectedItem.ToString().Split('-');
                    toDisplay = toDisplay.FindAll((x => x.Worker.PESEL == pesel[0].Remove(pesel[0].Length - 1)));
                }
                filterGroupBox.Text = "Filtr [AKTYWNY]";
                toDisplay = toDisplay.FindAll(x => (x.Day >= fromDatePicker.Value) && (x.Day <= toDatePicker.Value));
                refreshDutyList(toDisplay);
            }

        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            try
            {
                ListView.SelectedListViewItemCollection dutyToDelete = this.dutyListView.SelectedItems;
                DateTime day = tools.Tools.DateFromString(dutyToDelete[0].SubItems[0].Text);
                string pesel = dutyToDelete[0].SubItems[1].Text;

                DialogResult warning = MessageBox.Show("Czy na pewno chcesz usunąć wybrany dyżur?", "Ostrzeżenie", MessageBoxButtons.YesNo, MessageBoxIcon.Asterisk);

                if (warning == DialogResult.Yes)
                {
                    core.Duty selected = this.dutyList.Find(x => (x.Day == day) && (x.Worker.PESEL == pesel));
                    selected.Delete();
                    this.dutyList.Remove(selected);
                    refreshDutyList(this.dutyList);
                }
            }
            catch (ArgumentOutOfRangeException ex)
            {
                MessageBox.Show("Aby wykonać tę operację musisz zaznaczyć dyżur...", "Wystąpił błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
    }
}
