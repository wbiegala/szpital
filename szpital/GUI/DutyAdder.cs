﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace szpital
{
    public partial class DutyAdder : Form
    {
        List<core.Duty> duties;
        List<core.IMedicalStaff> medicals;

        public delegate void DutyAddedEventHandler(object o, core.Duty duty);
        public event DutyAddedEventHandler DutyAdded;


        public DutyAdder(List<core.Duty> duties, List<core.IMedicalStaff> medicals)
        {
            InitializeComponent();
            this.duties = new List<core.Duty>(duties);
            this.medicals = new List<core.IMedicalStaff>(medicals);
            dateDatePicker.MinDate = DateTime.Today.AddDays(1);
            dateDatePicker.Value = DateTime.Today.AddDays(7);
            refreshWorkerComboBox(getMedicalsToAddDuty());
        }

        private void dateDatePicker_ValueChanged(object sender, EventArgs e)
        {
            refreshWorkerComboBox(getMedicalsToAddDuty());
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private List<core.IMedicalStaff> getMedicalsToAddDuty()
        {
            List<core.IMedicalStaff> result = new List<core.IMedicalStaff>();

            foreach (var worker in medicals)
            {
                if (
                    (!duties.Exists(x => (x.Day == dateDatePicker.Value) && x.Worker.PESEL == worker.PESEL)) &&
                    (!duties.Exists(x => (x.Day == dateDatePicker.Value.AddDays(1) && x.Worker.PESEL == worker.PESEL))) &&
                    (!duties.Exists(x => (x.Day == dateDatePicker.Value.AddDays(-1) && x.Worker.PESEL == worker.PESEL))) &&
                    ((duties.FindAll(x => (x.Worker.PESEL == worker.PESEL) && (x.Day.Month == dateDatePicker.Value.Month)).Count <= 10)) &&
                    (!duties.Exists(x => (x.Worker.Spec == worker.Spec) && (x.Day == dateDatePicker.Value) && (x.Worker.Role.Name == "Lekarz")))
                    )
                {
                    result.Add(worker);
                }
            }

            return result;
        }

        private void refreshWorkerComboBox(List<core.IMedicalStaff> toDisplay)
        {
            medicalsComboBox.Items.Clear();
            foreach (var worker in toDisplay)
            {
                medicalsComboBox.Items.Add(worker.PESEL + " - " + worker.FirstName + " - " + worker.LastName + " - " + worker.Role.Name.ToString() + " - " + worker.Spec);
            }
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            try
            {
                string[] pesel = medicalsComboBox.SelectedItem.ToString().Split('-');
                core.Duty toAdd = new core.Duty(this.dateDatePicker.Value, medicals.Find(x => x.PESEL == pesel[0].Remove(pesel[0].Length - 1)));
                toAdd.Insert();
                DutyAdded(this, toAdd);
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        protected virtual void OnUserDataChanged(core.Duty duty)
        {
            DutyAdded(this, duty);
        }
    }
}
