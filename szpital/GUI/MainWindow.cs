﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Oracle.ManagedDataAccess.Client;

namespace szpital
{
    public partial class MainWindow : Form
    {
        private core.Worker current;

        public MainWindow(List<Exception> problems)
        {
            InitializeComponent();

            foreach (var problem in problems)
            {
                MessageBox.Show(problem.ToString(), "Błąd krytyczny!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            if (Program.Connection == null)
            {
                databaseStripStatusLabel.Text += "BŁĄD";
                databaseStripStatusLabel.ForeColor = Color.Red;
            }
            else
            {
                databaseStripStatusLabel.Text += "OK";
                databaseStripStatusLabel.ForeColor = Color.Green;
                ShowLoginForm();
            }
        }


        private void ShowLoginForm()
        {
            LoginForm login = new LoginForm();
            login.MdiParent = this;
            login.UserLogged += OnUserLogged;
            login.Show();
        }

        public void OnUserLogged(object o, core.Worker user)
        {
            this.MainMenu.Enabled = true;
            this.current = user;
            loggedStripStatusLabel.Text = "Zalogowany: " + current.Username + ", czas logowania:  " + DateTime.Now.ToString("yyyy/MM/dd hh:mm:ss");
        }

        private void logoutMainMenuItem_Click(object sender, EventArgs e)
        {
            foreach (var window in this.MdiChildren)
            {
                window.Close();
            }
            this.current = null;
            loggedStripStatusLabel.Text = "Wylogowany...";
            this.MainMenu.Enabled = false;
            ShowLoginForm();
        }

        private void closeMainMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void aboutMainMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void MainWindow_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.current != null)
            {
                switch (MessageBox.Show("Czy na pewno chcesz wyjść w programu?", "Zamykanie programu...", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                {
                    case DialogResult.No:
                        e.Cancel = true;
                        break;

                    default:
                        break;
                }
            }
        }

        private void changePasswordMainMenuItem_Click(object sender, EventArgs e)
        {
            foreach (var window in this.MdiChildren)
            {
                window.Close();
            }

            this.MainMenu.Enabled = false;
            WorkerForm currentWorker = new WorkerForm(this.current);
            currentWorker.UserDataChanged += OnUserDataChanged;
            currentWorker.FormClosed += OnWorkerFormClosed;
            currentWorker.MdiParent = this;
            currentWorker.Show();
        }


        public void OnUserDataChanged(object o, core.Worker user)
        {
            this.MainMenu.Enabled = true;
            loggedStripStatusLabel.Text = "Zalogowany: " + current.Username + ", czas logowania:  " + DateTime.Now.ToString("yyyy/MM/dd hh:mm:ss");
        }

        private void employeeMainMenuItem_Click(object sender, EventArgs e)
        {
            WorkerBrowser workerBrowser = new WorkerBrowser(!current.Role.IsMedicalStaff);
            workerBrowser.MdiParent = this;
            workerBrowser.Show();
        }

        private void dutiesMainMenuItem_Click(object sender, EventArgs e)
        {
            DutyBrowser dutyBrowser = new DutyBrowser(!current.Role.IsMedicalStaff);
            dutyBrowser.MdiParent = this;
            dutyBrowser.Show();
        }

        public void OnWorkerFormClosed(object o, EventArgs e)
        {
            this.MainMenu.Enabled = true;
        }


    }




}

