﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Oracle.ManagedDataAccess.Client;

namespace szpital
{
    public partial class WorkerForm : Form
    {
        bool isNewWorker;
        bool setPasswordMode;
        private core.Worker current;

        public delegate void UserDataChangedEventHandler(object o, core.Worker user);
        public event UserDataChangedEventHandler UserDataChanged;

        public WorkerForm()
        {
            InitializeComponent();
            Text = "Nowy pracownik";
            changePasswordButton.Visible = false;
            peselTextBox.ReadOnly = false;
            newPasswordLabel.Visible = true;
            newPasswordTextBox.Visible = true;
            confirmPasswordLabel.Visible = true;
            confirmPasswordTextBox.Visible = true;
            this.isNewWorker = true;
            setPasswordMode = true;
            setSpecsToComboBox();
            setRolesToComboBox();
        }

        public WorkerForm(core.Worker toDisplay)
        {
            if (toDisplay.Role.IsMedicalStaff == true)
            {
                MessageBox.Show("Zmiana roli dla pracowników medycznych lub zmiana specjalizacji lekarzom będzie skutkowała usunięciem ich dyżurów", "Ostrzeżenie", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            InitializeComponent();
            isNewWorker = false;
            setPasswordMode = false;
            this.current = toDisplay;
            usernameTextBox.Enabled = false;
            Text = "Pracownik - " + current.PESEL + " - " + current.FirstName + " " + current.LastName;
            setSpecsToComboBox();
            setRolesToComboBox();
            synchronizeControls();
        }


        private void synchronizeControls()
        {
            peselTextBox.Text = current.PESEL;
            firstNameTextBox.Text = current.FirstName;
            lastNameTextBox.Text = current.LastName;
            usernameTextBox.Text = current.Username;
            roleComboBox.SelectedItem = current.Role.Name;

            if (current.Role.Name == "Lekarz")
            {
                try
                {
                    extendedDataGroupBox.Visible = true;
                    string SQL = "select dd.LICENCE, ds.NAME as SPEC from DOCTOR_DATA dd " +
                                 "inner join DOC_SPEC ds on ds.ID=dd.DOC_SPEC_ID " +
                                 "where dd.WORKER_PESEL='" + current.PESEL + "'";

                    using (OracleCommand command = new OracleCommand(SQL, Program.Connection.Database))
                    {
                        OracleDataReader reader = command.ExecuteReader();
                        reader.Read();
                        licenceTextBox.Text = reader["LICENCE"].ToString();
                        specComboBox.SelectedItem = reader["SPEC"].ToString();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString(), "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }

        }

        // sprawdzenie czy rola == doctor :)
        private void roleComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (roleComboBox.SelectedItem.ToString() == "Lekarz")
            {
                extendedDataGroupBox.Visible = true;
            }
            else
            {
                extendedDataGroupBox.Visible = false;
            }
        }


        private void setRolesToComboBox()
        {
            foreach (var role in Program.Roles)
            {
                roleComboBox.Items.Add(role.Name);
            }

            if (isNewWorker == true)
                roleComboBox.SelectedItem = roleComboBox.Items[0];
        }


        private void setSpecsToComboBox()
        {
            try
            {
                List<string> items = new List<string>();
                string SQL = "select NAME from DOC_SPEC";
                using (OracleCommand command = new OracleCommand(SQL, Program.Connection.Database))
                {
                    OracleDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        items.Add(reader["NAME"].ToString());
                    }
                }

                foreach (var item in items)
                {
                    specComboBox.Items.Add(item);
                }

                if (isNewWorker == true)
                    specComboBox.SelectedItem = specComboBox.Items[0];
                if ((current == null) || (current.Role.Name != "Lekarz"))
                    specComboBox.SelectedItem = specComboBox.Items[0];
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void changePasswordButton_Click(object sender, EventArgs e)
        {
            setPasswordMode = true;
            newPasswordLabel.Visible = true;
            newPasswordTextBox.Visible = true;
            confirmPasswordLabel.Visible = true;
            confirmPasswordTextBox.Visible = true;
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (validateForm() == true)
                {
                    if (isNewWorker == false)
                    {
                        string SQL;

                        if ((roleComboBox.SelectedItem.ToString() == "Lekarz") && (current.Role.Name != "Lekarz"))
                        {
                            SQL = "insert into DOCTOR_DATA (WORKER_PESEL, LICENCE, DOC_SPEC_ID) values(" +
                                "'" + peselTextBox.Text + "', " +
                                "'" + licenceTextBox.Text + "', " +
                                "(select ID from DOC_SPEC where NAME='" + specComboBox.SelectedItem.ToString() + "'))";
                        }
                        else if ((roleComboBox.SelectedItem.ToString() == "Lekarz") && (current.Role.Name == "Lekarz"))
                        {
                            SQL = "update DOCTOR_DATA set LICENCE = '" + licenceTextBox.Text +
                                "', DOC_SPEC_ID=(select ID from DOC_SPEC where NAME='" + specComboBox.SelectedItem.ToString() +
                                "') where WORKER_PESEL = '" + peselTextBox.Text + "'";
                        }
                        else
                        {
                            SQL = "delete from DOCTOR_DATA where WORKER_PESEL = " + current.PESEL;
                        }

                        using (OracleCommand command = new OracleCommand(SQL, Program.Connection.Database))
                        {
                            command.ExecuteNonQuery();
                        }


                        if (this.setPasswordMode == true)
                        {
                            current.Update(firstNameTextBox.Text, lastNameTextBox.Text, usernameTextBox.Text, tools.Tools.GetMD5(newPasswordTextBox.Text), Program.Roles.Find(x => x.Name == roleComboBox.SelectedItem.ToString()));
                        }
                        else
                        {
                            current.Update(firstNameTextBox.Text, lastNameTextBox.Text, usernameTextBox.Text, current.Password, Program.Roles.Find(x => x.Name == roleComboBox.SelectedItem.ToString()));
                        }

                        OnUserDataChanged(current);

                    }
                    else
                    {
                        if (roleComboBox.SelectedItem.ToString() == "Lekarz")
                        {
                            string SQL = "insert into DOCTOR_DATA (WORKER_PESEL, LICENCE, DOC_SPEC_ID) values(" +
                                "'" + peselTextBox.Text + "', " +
                                "'" + licenceTextBox.Text + "', " +
                                "(select ID from DOC_SPEC where NAME='" + specComboBox.SelectedItem.ToString() + "'))";

                            using (OracleCommand command = new OracleCommand(SQL, Program.Connection.Database))
                            {
                                command.ExecuteNonQuery();
                            }
                        }

                        this.current = new core.Worker(peselTextBox.Text, firstNameTextBox.Text, lastNameTextBox.Text, usernameTextBox.Text,
                            tools.Tools.GetMD5(newPasswordTextBox.Text), Program.Roles.Find(x => x.Name == roleComboBox.SelectedItem.ToString()));

                        current.Insert();

                        OnUserDataChanged(current);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Close();
            }

        }



        private bool validateForm()
        {
            int errors = 0;

            try
            {

                // spr pesel
                if (tools.Tools.CheckPesel(peselTextBox.Text) == false)
                {
                    MessageBox.Show("Błędny numer PESEL", "Błędne dane", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    errors++;
                }

                if (isNewWorker == true)
                {
                    using (OracleCommand command = new OracleCommand("select count(*) as count from WORKER where PESEL='" + peselTextBox.Text + "'", Program.Connection.Database))
                    {
                        OracleDataReader reader = command.ExecuteReader();
                        reader.Read();
                        if (Int32.Parse(reader["count"].ToString()) != 0)
                        {
                            MessageBox.Show("W bazie istnieje już użytkownik z takim numerem PESEL", "Błędne dane", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            errors++;
                        }
                    }
                }

                // spr unikalność loginu i długość
                if (usernameTextBox.Text.Length < 6)
                {
                    MessageBox.Show("Proponowana nazwa użytkownika jest za krótka- min. długość to 6 znaków!", "Błędne dane", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    errors++;
                }

                if (isNewWorker == true)
                {
                    using (OracleCommand command = new OracleCommand("select count(*) as count from WORKER where USERNAME='" + usernameTextBox.Text + "'", Program.Connection.Database))
                    {
                        OracleDataReader reader = command.ExecuteReader();
                        reader.Read();
                        if (Int32.Parse(reader["count"].ToString()) != 0)
                        {
                            MessageBox.Show("W bazie istnieje już użytkownik z takią nazwą", "Błędne dane", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            errors++;
                        }
                    }
                }

                if (this.setPasswordMode == true)
                {
                    if (newPasswordTextBox.Text != confirmPasswordTextBox.Text)
                    {
                        MessageBox.Show("Wpisane hasła są różne od siebie!", "Błędne dane", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        errors++;
                    }
                    if (newPasswordTextBox.Text.Length < 8)
                    {
                        MessageBox.Show("Proponowane hasło jest za krótkie - min. długość to 8 znaków!", "Błędne dane", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        errors++;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            if (errors == 0)
                return true;
            else
                return false;


        }


        /// <summary>
        /// Publisher for event UserLogged. 
        /// </summary>
        /// <param name="user"></param>
        protected virtual void OnUserDataChanged(core.Worker user)
        {
            UserDataChanged(this, user);
        }



    }
}
