﻿namespace szpital
{
    partial class DutyBrowser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dutyListView = new System.Windows.Forms.ListView();
            this.addButton = new System.Windows.Forms.Button();
            this.filterGroupBox = new System.Windows.Forms.GroupBox();
            this.workerCheckBox = new System.Windows.Forms.CheckBox();
            this.medicalComboBox = new System.Windows.Forms.ComboBox();
            this.clearButton = new System.Windows.Forms.Button();
            this.toDateLabel = new System.Windows.Forms.Label();
            this.fromDateLabel = new System.Windows.Forms.Label();
            this.searchButton = new System.Windows.Forms.Button();
            this.toDatePicker = new System.Windows.Forms.DateTimePicker();
            this.fromDatePicker = new System.Windows.Forms.DateTimePicker();
            this.deleteButton = new System.Windows.Forms.Button();
            this.filterGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // dutyListView
            // 
            this.dutyListView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dutyListView.FullRowSelect = true;
            this.dutyListView.Location = new System.Drawing.Point(12, 112);
            this.dutyListView.MultiSelect = false;
            this.dutyListView.Name = "dutyListView";
            this.dutyListView.Size = new System.Drawing.Size(938, 352);
            this.dutyListView.TabIndex = 0;
            this.dutyListView.UseCompatibleStateImageBehavior = false;
            // 
            // addButton
            // 
            this.addButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.addButton.Location = new System.Drawing.Point(875, 77);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(75, 23);
            this.addButton.TabIndex = 1;
            this.addButton.Text = "Dodaj";
            this.addButton.UseVisualStyleBackColor = true;
            this.addButton.Click += new System.EventHandler(this.addButton_Click);
            // 
            // filterGroupBox
            // 
            this.filterGroupBox.Controls.Add(this.workerCheckBox);
            this.filterGroupBox.Controls.Add(this.medicalComboBox);
            this.filterGroupBox.Controls.Add(this.clearButton);
            this.filterGroupBox.Controls.Add(this.toDateLabel);
            this.filterGroupBox.Controls.Add(this.fromDateLabel);
            this.filterGroupBox.Controls.Add(this.searchButton);
            this.filterGroupBox.Controls.Add(this.toDatePicker);
            this.filterGroupBox.Controls.Add(this.fromDatePicker);
            this.filterGroupBox.Location = new System.Drawing.Point(12, 12);
            this.filterGroupBox.Name = "filterGroupBox";
            this.filterGroupBox.Size = new System.Drawing.Size(585, 94);
            this.filterGroupBox.TabIndex = 2;
            this.filterGroupBox.TabStop = false;
            this.filterGroupBox.Text = "Filtr [nieaktywny]";
            // 
            // workerCheckBox
            // 
            this.workerCheckBox.AutoSize = true;
            this.workerCheckBox.Location = new System.Drawing.Point(62, 67);
            this.workerCheckBox.Name = "workerCheckBox";
            this.workerCheckBox.Size = new System.Drawing.Size(79, 17);
            this.workerCheckBox.TabIndex = 8;
            this.workerCheckBox.Text = "Pracownik:";
            this.workerCheckBox.UseVisualStyleBackColor = true;
            // 
            // medicalComboBox
            // 
            this.medicalComboBox.FormattingEnabled = true;
            this.medicalComboBox.Location = new System.Drawing.Point(147, 65);
            this.medicalComboBox.Name = "medicalComboBox";
            this.medicalComboBox.Size = new System.Drawing.Size(270, 21);
            this.medicalComboBox.TabIndex = 6;
            // 
            // clearButton
            // 
            this.clearButton.Location = new System.Drawing.Point(423, 65);
            this.clearButton.Name = "clearButton";
            this.clearButton.Size = new System.Drawing.Size(75, 23);
            this.clearButton.TabIndex = 5;
            this.clearButton.Text = "Wyczyść";
            this.clearButton.UseVisualStyleBackColor = true;
            this.clearButton.Click += new System.EventHandler(this.clearButton_Click);
            // 
            // toDateLabel
            // 
            this.toDateLabel.AutoSize = true;
            this.toDateLabel.Location = new System.Drawing.Point(258, 23);
            this.toDateLabel.Name = "toDateLabel";
            this.toDateLabel.Size = new System.Drawing.Size(24, 13);
            this.toDateLabel.TabIndex = 4;
            this.toDateLabel.Text = "Do:";
            // 
            // fromDateLabel
            // 
            this.fromDateLabel.AutoSize = true;
            this.fromDateLabel.Location = new System.Drawing.Point(6, 23);
            this.fromDateLabel.Name = "fromDateLabel";
            this.fromDateLabel.Size = new System.Drawing.Size(24, 13);
            this.fromDateLabel.TabIndex = 3;
            this.fromDateLabel.Text = "Od:";
            // 
            // searchButton
            // 
            this.searchButton.Location = new System.Drawing.Point(504, 65);
            this.searchButton.Name = "searchButton";
            this.searchButton.Size = new System.Drawing.Size(75, 23);
            this.searchButton.TabIndex = 2;
            this.searchButton.Text = "Filtruj";
            this.searchButton.UseVisualStyleBackColor = true;
            this.searchButton.Click += new System.EventHandler(this.searchButton_Click);
            // 
            // toDatePicker
            // 
            this.toDatePicker.Location = new System.Drawing.Point(261, 39);
            this.toDatePicker.Name = "toDatePicker";
            this.toDatePicker.Size = new System.Drawing.Size(240, 20);
            this.toDatePicker.TabIndex = 1;
            // 
            // fromDatePicker
            // 
            this.fromDatePicker.Location = new System.Drawing.Point(6, 39);
            this.fromDatePicker.Name = "fromDatePicker";
            this.fromDatePicker.Size = new System.Drawing.Size(240, 20);
            this.fromDatePicker.TabIndex = 0;
            // 
            // deleteButton
            // 
            this.deleteButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.deleteButton.Location = new System.Drawing.Point(794, 77);
            this.deleteButton.Name = "deleteButton";
            this.deleteButton.Size = new System.Drawing.Size(75, 23);
            this.deleteButton.TabIndex = 3;
            this.deleteButton.Text = "Usuń";
            this.deleteButton.UseVisualStyleBackColor = true;
            this.deleteButton.Click += new System.EventHandler(this.deleteButton_Click);
            // 
            // DutyBrowser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(962, 476);
            this.Controls.Add(this.deleteButton);
            this.Controls.Add(this.filterGroupBox);
            this.Controls.Add(this.addButton);
            this.Controls.Add(this.dutyListView);
            this.Name = "DutyBrowser";
            this.Text = "Przeglądarka dyżurów";
            this.filterGroupBox.ResumeLayout(false);
            this.filterGroupBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView dutyListView;
        private System.Windows.Forms.Button addButton;
        private System.Windows.Forms.GroupBox filterGroupBox;
        private System.Windows.Forms.Button clearButton;
        private System.Windows.Forms.Label toDateLabel;
        private System.Windows.Forms.Label fromDateLabel;
        private System.Windows.Forms.Button searchButton;
        private System.Windows.Forms.DateTimePicker toDatePicker;
        private System.Windows.Forms.DateTimePicker fromDatePicker;
        private System.Windows.Forms.Button deleteButton;
        private System.Windows.Forms.ComboBox medicalComboBox;
        private System.Windows.Forms.CheckBox workerCheckBox;
    }
}