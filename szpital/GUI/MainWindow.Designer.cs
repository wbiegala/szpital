﻿namespace szpital
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MainMenu = new System.Windows.Forms.MenuStrip();
            this.programToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.changePasswordMainMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logoutMainMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeMainMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.spisyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.employeeMainMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dutiesMainMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.MainMenuStatusStrip = new System.Windows.Forms.StatusStrip();
            this.databaseStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.separatorStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.loggedStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.MainMenu.SuspendLayout();
            this.MainMenuStatusStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // MainMenu
            // 
            this.MainMenu.Enabled = false;
            this.MainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.programToolStripMenuItem,
            this.spisyToolStripMenuItem});
            this.MainMenu.Location = new System.Drawing.Point(0, 0);
            this.MainMenu.Name = "MainMenu";
            this.MainMenu.Size = new System.Drawing.Size(784, 24);
            this.MainMenu.TabIndex = 1;
            this.MainMenu.Text = "menuStrip1";
            // 
            // programToolStripMenuItem
            // 
            this.programToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.changePasswordMainMenuItem,
            this.logoutMainMenuItem,
            this.toolStripSeparator1,
            this.closeMainMenuItem});
            this.programToolStripMenuItem.Name = "programToolStripMenuItem";
            this.programToolStripMenuItem.Size = new System.Drawing.Size(65, 20);
            this.programToolStripMenuItem.Text = "Program";
            // 
            // changePasswordMainMenuItem
            // 
            this.changePasswordMainMenuItem.Name = "changePasswordMainMenuItem";
            this.changePasswordMainMenuItem.Size = new System.Drawing.Size(206, 22);
            this.changePasswordMainMenuItem.Text = "Zmień dane użytkownika";
            this.changePasswordMainMenuItem.Click += new System.EventHandler(this.changePasswordMainMenuItem_Click);
            // 
            // logoutMainMenuItem
            // 
            this.logoutMainMenuItem.Name = "logoutMainMenuItem";
            this.logoutMainMenuItem.Size = new System.Drawing.Size(206, 22);
            this.logoutMainMenuItem.Text = "Wyloguj";
            this.logoutMainMenuItem.Click += new System.EventHandler(this.logoutMainMenuItem_Click);
            // 
            // closeMainMenuItem
            // 
            this.closeMainMenuItem.Name = "closeMainMenuItem";
            this.closeMainMenuItem.Size = new System.Drawing.Size(206, 22);
            this.closeMainMenuItem.Text = "Zamknij";
            this.closeMainMenuItem.Click += new System.EventHandler(this.closeMainMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(203, 6);
            // 
            // spisyToolStripMenuItem
            // 
            this.spisyToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.employeeMainMenuItem,
            this.dutiesMainMenuItem});
            this.spisyToolStripMenuItem.Name = "spisyToolStripMenuItem";
            this.spisyToolStripMenuItem.Size = new System.Drawing.Size(46, 20);
            this.spisyToolStripMenuItem.Text = "Spisy";
            // 
            // employeeMainMenuItem
            // 
            this.employeeMainMenuItem.Name = "employeeMainMenuItem";
            this.employeeMainMenuItem.Size = new System.Drawing.Size(135, 22);
            this.employeeMainMenuItem.Text = "Pracownicy";
            this.employeeMainMenuItem.Click += new System.EventHandler(this.employeeMainMenuItem_Click);
            // 
            // dutiesMainMenuItem
            // 
            this.dutiesMainMenuItem.Name = "dutiesMainMenuItem";
            this.dutiesMainMenuItem.Size = new System.Drawing.Size(135, 22);
            this.dutiesMainMenuItem.Text = "Dyżury";
            this.dutiesMainMenuItem.Click += new System.EventHandler(this.dutiesMainMenuItem_Click);
            // 
            // MainMenuStatusStrip
            // 
            this.MainMenuStatusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.databaseStripStatusLabel,
            this.separatorStripStatusLabel,
            this.loggedStripStatusLabel});
            this.MainMenuStatusStrip.Location = new System.Drawing.Point(0, 539);
            this.MainMenuStatusStrip.Name = "MainMenuStatusStrip";
            this.MainMenuStatusStrip.Size = new System.Drawing.Size(784, 22);
            this.MainMenuStatusStrip.TabIndex = 3;
            this.MainMenuStatusStrip.Text = "statusStrip1";
            // 
            // databaseStripStatusLabel
            // 
            this.databaseStripStatusLabel.Name = "databaseStripStatusLabel";
            this.databaseStripStatusLabel.Size = new System.Drawing.Size(105, 17);
            this.databaseStripStatusLabel.Text = "Stan bazy danych: ";
            // 
            // separatorStripStatusLabel
            // 
            this.separatorStripStatusLabel.Name = "separatorStripStatusLabel";
            this.separatorStripStatusLabel.Size = new System.Drawing.Size(10, 17);
            this.separatorStripStatusLabel.Text = "|";
            // 
            // loggedStripStatusLabel
            // 
            this.loggedStripStatusLabel.Name = "loggedStripStatusLabel";
            this.loggedStripStatusLabel.Size = new System.Drawing.Size(85, 17);
            this.loggedStripStatusLabel.Text = "Wylogowany...";
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Controls.Add(this.MainMenuStatusStrip);
            this.Controls.Add(this.MainMenu);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.MainMenu;
            this.Name = "MainWindow";
            this.Text = "Szpital";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainWindow_FormClosing);
            this.MainMenu.ResumeLayout(false);
            this.MainMenu.PerformLayout();
            this.MainMenuStatusStrip.ResumeLayout(false);
            this.MainMenuStatusStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip MainMenu;
        private System.Windows.Forms.ToolStripMenuItem programToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem changePasswordMainMenuItem;
        private System.Windows.Forms.ToolStripMenuItem logoutMainMenuItem;
        private System.Windows.Forms.ToolStripMenuItem closeMainMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem spisyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem employeeMainMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dutiesMainMenuItem;
        private System.Windows.Forms.StatusStrip MainMenuStatusStrip;
        private System.Windows.Forms.ToolStripStatusLabel databaseStripStatusLabel;
        private System.Windows.Forms.ToolStripStatusLabel separatorStripStatusLabel;
        private System.Windows.Forms.ToolStripStatusLabel loggedStripStatusLabel;
    }
}

