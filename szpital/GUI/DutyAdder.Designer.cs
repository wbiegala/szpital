﻿namespace szpital
{
    partial class DutyAdder
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.addButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.dateDatePicker = new System.Windows.Forms.DateTimePicker();
            this.medicalsComboBox = new System.Windows.Forms.ComboBox();
            this.dateLabel = new System.Windows.Forms.Label();
            this.workerLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // addButton
            // 
            this.addButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.addButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addButton.Location = new System.Drawing.Point(270, 115);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(75, 23);
            this.addButton.TabIndex = 0;
            this.addButton.Text = "Dodaj";
            this.addButton.UseVisualStyleBackColor = true;
            this.addButton.Click += new System.EventHandler(this.addButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Location = new System.Drawing.Point(189, 115);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 1;
            this.cancelButton.Text = "Anuluj";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // dateDatePicker
            // 
            this.dateDatePicker.Location = new System.Drawing.Point(12, 27);
            this.dateDatePicker.Name = "dateDatePicker";
            this.dateDatePicker.Size = new System.Drawing.Size(333, 20);
            this.dateDatePicker.TabIndex = 2;
            this.dateDatePicker.ValueChanged += new System.EventHandler(this.dateDatePicker_ValueChanged);
            // 
            // medicalsComboBox
            // 
            this.medicalsComboBox.FormattingEnabled = true;
            this.medicalsComboBox.Location = new System.Drawing.Point(12, 72);
            this.medicalsComboBox.Name = "medicalsComboBox";
            this.medicalsComboBox.Size = new System.Drawing.Size(333, 21);
            this.medicalsComboBox.TabIndex = 3;
            // 
            // dateLabel
            // 
            this.dateLabel.AutoSize = true;
            this.dateLabel.Location = new System.Drawing.Point(12, 11);
            this.dateLabel.Name = "dateLabel";
            this.dateLabel.Size = new System.Drawing.Size(30, 13);
            this.dateLabel.TabIndex = 4;
            this.dateLabel.Text = "Data";
            // 
            // workerLabel
            // 
            this.workerLabel.AutoSize = true;
            this.workerLabel.Location = new System.Drawing.Point(12, 56);
            this.workerLabel.Name = "workerLabel";
            this.workerLabel.Size = new System.Drawing.Size(57, 13);
            this.workerLabel.TabIndex = 5;
            this.workerLabel.Text = "Pracownik";
            // 
            // DutyAdder
            // 
            this.AcceptButton = this.addButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size(357, 150);
            this.Controls.Add(this.workerLabel);
            this.Controls.Add(this.dateLabel);
            this.Controls.Add(this.medicalsComboBox);
            this.Controls.Add(this.dateDatePicker);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.addButton);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(373, 189);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(373, 189);
            this.Name = "DutyAdder";
            this.Text = "Nowy dyżur";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button addButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.DateTimePicker dateDatePicker;
        private System.Windows.Forms.ComboBox medicalsComboBox;
        private System.Windows.Forms.Label dateLabel;
        private System.Windows.Forms.Label workerLabel;
    }
}