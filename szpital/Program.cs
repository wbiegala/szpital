﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Oracle.ManagedDataAccess.Client;

namespace szpital
{
    static class Program
    {

        public static database.OracleDriver Connection;
        public static List<core.Role> Roles;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            List<Exception> startProblems = new List<Exception>();
            Roles = new List<core.Role>();
            try
            {
                Connection = new database.OracleDriver();

                string SQL = "select * from ROLE";
                using (OracleCommand command = new OracleCommand(SQL, Connection.Database))
                {
                    OracleDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        int id = Int32.Parse(reader["ID"].ToString());
                        string name = reader["NAME"].ToString();
                        bool isMedicalStaff;
                        if (reader["IS_MEDICAL_STAFF"].ToString() == "T")
                            isMedicalStaff = true;
                        else
                            isMedicalStaff = false;
                        Roles.Add(new core.Role(id, name, isMedicalStaff));
                    }
                }
            }
            catch (database.CannotConnectToDatabaseException exception)
            {
                startProblems.Add(exception);
            }
            catch (Exception ex)
            {
                startProblems.Add(ex);
            }


            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainWindow(startProblems));
        }
    }
}
