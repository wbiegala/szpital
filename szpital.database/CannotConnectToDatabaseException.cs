﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace szpital.database
{
    public class CannotConnectToDatabaseException : ApplicationException
    {
        private string reason;
        private string obj;
        private DateTime time;
        Exception internalOracleException;

        public string Reason { get => reason; }
        public string Obj { get => obj; }
        public DateTime Time { get => time; }
        public Exception InternalOracleException { get => internalOracleException; }

        public CannotConnectToDatabaseException(string reason, string obj)
        {
            this.time = DateTime.Now;
            this.reason = reason;
            this.obj = obj;
        }

        public CannotConnectToDatabaseException(string reason, string obj, Exception exception)
        {
            this.time = DateTime.Now;
            this.reason = reason;
            this.obj = obj;
            this.internalOracleException = exception;
        }


    }
}
