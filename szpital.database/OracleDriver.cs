﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Oracle.ManagedDataAccess.Client;
using System.IO;

namespace szpital.database
{
    public class OracleDriver
    {
        private string host, port, serviceName, username, password;
        private OracleConnection database;

        private string ConnectionString { get { return
                    "Data Source=" + host +
                    "/" + serviceName +
                    ":" + port + ";" +
                    "User Id=" + username + ";" +
                    "Password=" + password + ";"; } }

        public OracleConnection Database { get => database; }

        /// <summary>
        /// Read all data from database.config
        /// </summary>
        public OracleDriver()
        {
            try
            {
                using (FileStream fs = new FileStream("database.config", FileMode.Open, FileAccess.Read))
                {
                    using (StreamReader sr = new StreamReader(fs))
                    {
                        while (!sr.EndOfStream)
                        {
                            string[] key_value = sr.ReadLine().Split('=');

                            switch (key_value[0])
                            {
                                case "host":
                                    this.host = key_value[1];
                                    break;
                                case "port":
                                    this.port = key_value[1];
                                    break;
                                case "service_name":
                                    this.serviceName = key_value[1];
                                    break;
                                case "username":
                                    this.username = key_value[1];
                                    break;
                                case "password":
                                    this.password = key_value[1];
                                    break;
                            }
                        }
                    }
                }
            }

            catch (FileNotFoundException ex)
            {
                throw new CannotConnectToDatabaseException("Nie ma pliku z konfiguracją bazy danych", this.GetType().ToString(), ex);
            }
            catch (IOException ex)
            {
                throw new CannotConnectToDatabaseException("Błąd odczytu pliku", this.GetType().ToString(), ex);
            }
            catch (OutOfMemoryException ex)
            {
                throw new CannotConnectToDatabaseException("Brak dostępnej pamięci", this.GetType().ToString(), ex);
            }

            if (
                (host.Length < 7) ||
                (serviceName.Length < 2) ||
                (port.Length < 2) ||
                (username.Length < 1) || 
                (password.Length < 1) 
                )
            {
                throw new CannotConnectToDatabaseException("Błędne dane w pliku konfiguracyjnym bazy danych", this.GetType().ToString());
            }


            try
            {
                this.database = new OracleConnection(this.ConnectionString);
                this.database.Open();
            }
            catch (Exception ex)
            {
                throw new CannotConnectToDatabaseException("Błąd połączenia z bazą danych", this.GetType().ToString(), ex);
            }


        }

    }
}
